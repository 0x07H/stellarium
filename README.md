# Stellarium
[![Build Status](https://travis-ci.org/0x07H/stellarium.svg?branch=master)](https://travis-ci.org/0x07H/stellarium)
[![Build status](https://ci.appveyor.com/api/projects/status/github/0x07H/stellarium?branch=master&svg=true)](https://ci.appveyor.com/project/0x07H/stellarium)
[![Coverity Scan](https://img.shields.io/coverity/scan/19410.svg)](https://scan.coverity.com/projects/0x07h-stellarium)
[![Coverage Status](https://coveralls.io/repos/github/0x07H/stellarium/badge.svg?branch=master)](https://coveralls.io/github/0x07H/stellarium?branch=master)
[![CodeFactor](https://www.codefactor.io/repository/github/0x07h/stellarium/badge)](https://www.codefactor.io/repository/github/0x07h/stellarium)
<br/>
[![Total alerts](https://img.shields.io/lgtm/alerts/g/Stellarium/stellarium.svg?logo=lgtm&logoWidth=18)](https://lgtm.com/projects/g/Stellarium/stellarium/alerts/)

Stellarium is a free open source planetarium for your computer. It shows a realistic sky
in 3D, just like what you see with the naked eye, binoculars or a telescope.


THIS IS A CLONED REPOSITORY. PLEASE FOLLOW THE LINK BELOW FOR THE ORIGINAL :

[www.stellarium.org](http://www.stellarium.org)
